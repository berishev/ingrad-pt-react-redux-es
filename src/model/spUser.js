import * as Parse from 'parse';

export const SPUserSexEnum = {
  male: 'male',
  female: 'female'
};

export class SPUser extends Parse.Object {
  static getQuery(query) {
    query = query || new Parse.Query(SPUser);
    query = query.include(['department']).ascending('LinkTitle');
    return query;
  }
  constructor() {
    super('SPUser');
  }
  get spID() {
    return this.get('spID');
  }
  set spID(value) {
    this.set('spID', value);
  }
  get metadata() {
    return this.get('metadata');
  }
  set metadata(value) {
    this.set('metadata', value);
  }
  get login() {
    return this.get('login');
  }
  set login(value) {
    this.set('login', value);
  }
  get firstname() {
    return this.get('firstname');
  }
  set firstname(value) {
    this.set('firstname', value);
  }
  get lastname() {
    return this.get('lastname');
  }
  set lastname(value) {
    this.set('lastname', value);
  }
  get patronymic() {
    return this.get('patronymic');
  }
  set patronymic(value) {
    this.set('patronymic', value);
  }
  get photo() {
    return this.get('photo');
  }
  set photo(value) {
    this.set('photo', value);
  }
  get position() {
    return this.get('position');
  }
  set position(value) {
    this.set('position', value);
  }
  get phoneWork() {
    return this.get('phoneWork');
  }
  set phoneWork(value) {
    this.set('phoneWork', value);
  }
  get personalPhone() {
    return this.get('personalPhone');
  }
  set personalPhone(value) {
    this.set('personalPhone', value);
  }
  get email() {
    return this.get('email');
  }
  set email(value) {
    this.set('email', value);
  }
  get idonec() {
    return this.get('idonec');
  }
  set idonec(value) {
    this.set('idonec', value);
  }
  get snils() {
    return this.get('snils');
  }
  set snils(value) {
    this.set('snils', value);
  }
  get inn() {
    return this.get('inn');
  }
  set inn(value) {
    this.set('inn', value);
  }
  get organization() {
    return this.get('organization');
  }
  set organization(value) {
    this.set('organization', value);
  }
  get currentEmployee() {
    return this.get('currentEmployee');
  }
  set currentEmployee(value) {
    this.set('currentEmployee', value);
  }
  get interests() {
    return this.get('interests');
  }
  set interests(value) {
    this.set('interests', value);
  }
  get sphere() {
    return this.get('sphere');
  }
  set sphere(value) {
    this.set('sphere', value);
  }
  get office() {
    return this.get('office');
  }
  set office(value) {
    this.set('office', value);
  }
  get sex() {
    return this.get('sex');
  }
  set sex(value) {
    this.set('sex', value);
  }
  get personalEmail() {
    return this.get('personalEmail');
  }
  set personalEmail(value) {
    this.set('personalEmail', value);
  }
  get otherContact() {
    return this.get('otherContact');
  }
  set otherContact(value) {
    this.set('otherContact', value);
  }
  get media() {
    return this.get('media');
  }
  set media(value) {
    this.set('media', value);
  }
  get articles() {
    return this.get('articles');
  }
  set articles(value) {
    this.set('articles', value);
  }
  get skills() {
    return this.get('skills');
  }
  set skills(value) {
    this.set('skills', value);
  }
  get phoneWorker() {
    return this.get('phoneWorker');
  }
  set phoneWorker(value) {
    this.set('phoneWorker', value);
  }
  get phoneAdditional() {
    return this.get('phoneAdditional');
  }
  set phoneAdditional(value) {
    this.set('phoneAdditional', value);
  }
  get department() {
    return this.get('department');
  }
  set department(value) {
    this.set('department', value);
  }
  get isArchive() {
    return this.get('isArchive');
  }
  set isArchive(value) {
    this.set('isArchive', value);
  }
  get hireDate() {
    return this.get('hireDate');
  }
  set hireDate(value) {
    this.set('hireDate', value);
  }
  get birthday() {
    return this.get('birthday');
  }
  set birthday(value) {
    this.set('birthday', value);
  }
  get hidetel() {
    return this.get('hidetel');
  }
  set hidetel(value) {
    this.set('hidetel', value);
  }
  get displayOnPortal() {
    return this.get('displayOnPortal');
  }
  set displayOnPortal(value) {
    this.set('displayOnPortal', value);
  }
  get floorNumber() {
    return this.get('floorNumber');
  }
  set floorNumber(value) {
    this.set('floorNumber', value);
  }
  get LinkTitle() {
    return this.get('LinkTitle');
  }
  set LinkTitle(value) {
    this.set('LinkTitle', value);
  }
}
Parse.Object.registerSubclass('SPUser', SPUser);
