import * as Parse from 'parse';

export class SPOrgStructure extends Parse.Object {
  static getQuery(query) {
    query = query || new Parse.Query(SPOrgStructure);
    query = query.descending('spID');
    return query;
  }
  constructor() {
    super('SPOrgStructure');
  }
  get spID() {
    return this.get('spID');
  }
  set spID(value) {
    this.set('spID', value);
  }
  get priority() {
    return this.get('priority');
  }
  set priority(value) {
    this.set('priority', value);
  }
  get isArchive() {
    return this.get('isArchive');
  }
  set isArchive(value) {
    this.set('isArchive', value);
  }
  get parentSPID() {
    return this.get('parentSPID');
  }
  set parentSPID(value) {
    this.set('parentSPID', value);
  }
  get LinkTitle() {
    return this.get('LinkTitle');
  }
  set LinkTitle(value) {
    this.set('LinkTitle', value);
  }
  get idEmployees() {
    return this.get('idEmployees');
  }
  set idEmployees(value) {
    this.set('idEmployees', value);
  }
  get assist() {
    return this.get('assist');
  }
  set assist(value) {
    this.set('assist', value);
  }
}
Parse.Object.registerSubclass('SPOrgStructure', SPOrgStructure);
