import * as Parse from 'parse';
import * as Model from './';

export class TeamUser extends Parse.Object {
  static getQuery(query) {
    query = query || new Parse.Query(Model.TeamUser);
    query = query.include(['team', 'projects', 'spUser']);
    return query;
  }
  constructor() {
    super('TeamUser');
  }
  get team() {
    return this.get('team');
  }
  set team(value) {
    this.set('team', value);
  }
  get position() {
    return this.get('position');
  }
  set position(value) {
    this.set('position', value);
  }
  get task() {
    return this.get('task');
  }
  set task(value) {
    this.set('task', value);
  }
  get lastname() {
    return this.get('lastname');
  }
  set lastname(value) {
    this.set('lastname', value);
  }
  get name() {
    return this.get('name');
  }
  set name(value) {
    this.set('name', value);
  }
  get patronymic() {
    return this.get('patronymic');
  }
  set patronymic(value) {
    this.set('patronymic', value);
  }
  get phoneCode() {
    return this.get('phoneCode');
  }
  set phoneCode(value) {
    this.set('phoneCode', value);
  }
  get phoneMobile() {
    return this.get('phoneMobile');
  }
  set phoneMobile(value) {
    this.set('phoneMobile', value);
  }
  get officeNumber() {
    return this.get('officeNumber');
  }
  set officeNumber(value) {
    this.set('officeNumber', value);
  }
  get floorOrRoom() {
    return this.get('floorOrRoom');
  }
  set floorOrRoom(value) {
    this.set('floorOrRoom', value);
  }
  get projects() {
    let value = this.get('projects');
    if (!value) this.set('projects', []);
    return this.get('projects');
  }
  set projects(value) {
    this.set('projects', value);
  }
  get spUser() {
    return this.get('spUser');
  }
  set spUser(value) {
    this.set('spUser', value);
  }
}

Parse.Object.registerSubclass('TeamUser', TeamUser);
