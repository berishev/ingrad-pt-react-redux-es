import * as Parse from 'parse';

export const ProjectRelTypeEnum = {
  M: 'M',
  MO: 'MO'
};

export const ProjectRelTypeLabels = {
  [ProjectRelTypeEnum.M]: 'Москва',
  [ProjectRelTypeEnum.MO]: 'Московская область'
};

export class Project extends Parse.Object {
  constructor() {
    super('Project');
  }

  get code() {
    return this.get('code');
  }

  set code(value) {
    this.set('code', value);
  }

  get name() {
    return this.get('name');
  }

  set name(value) {
    this.set('name', value);
  }
  get commercialName() {
    return this.get('commercialName');
  }
  set commercialName(value) {
    this.set('commercialName', value);
  }
  get iconUrl() {
    return this.get('iconUrl');
  }
  set iconUrl(value) {
    this.set('iconUrl', value);
  }
  get relType() {
    return this.get('relType');
  }
  set relType(value) {
    this.set('relType', value);
  }
}

Parse.Object.registerSubclass('Project', Project);
