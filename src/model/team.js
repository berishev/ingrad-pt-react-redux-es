import * as Parse from 'parse';

export class Team extends Parse.Object {
  static getQuery(query) {
    query = query || new Parse.Query(Team);
    query = query.include(['spOrg']).ascending('name');
    return query;
  }
  constructor() {
    super('Team');
  }
  get name() {
    return this.get('name');
  }
  set name(value) {
    this.set('name', value);
  }
  get fullname() {
    return this.get('fullname');
  }
  set fullname(value) {
    this.set('fullname', value);
  }
  get spOrg() {
    return this.get('spOrg');
  }
  set spOrg(value) {
    this.set('spOrg', value);
  }
  get positions() {
    let value = this.get('positions');
    if (!value) this.set('positions', []);
    return this.get('positions');
  }
  set positions(value) {
    this.set('positions', value);
  }
}

Parse.Object.registerSubclass('Team', Team);
