import {
  GET_PROJECT_REQUEST,
  POST_PROJECT_REQUEST,
  DEL_PROJECT_REQUEST,
  PUT_PROJECT_REQUEST,
  DISMISS_PROJECT_ERROR
} from '../constants';

export function get(project_id, step_id) {
  return dispatch => {
    dispatch({
      type: GET_PROJECT_REQUEST,
      project_id,
      step_id
    });
  };
}

export function add(doc) {
  return dispatch => {
    dispatch({
      type: POST_PROJECT_REQUEST,
      doc
    });
  };
}

export function save(doc) {
  return dispatch => {
    dispatch({
      type: PUT_PROJECT_REQUEST,
      doc
    });
  };
}

export function del(id) {
  return dispatch => {
    dispatch({
      type: DEL_PROJECT_REQUEST,
      id
    });
  };
}

export function dismissError() {
  return dispatch => {
    return dispatch({
      type: DISMISS_PROJECT_ERROR
    });
  };
}
