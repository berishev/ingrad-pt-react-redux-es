import {
  GET_PROJECT_REQUEST,
  GET_PROJECT_SUCCESS,
  PUT_PROJECT_SUCCESS,
  POST_PROJECT_SUCCESS,
  GET_PROJECT_ERROR,
  DISMISS_PROJECT_ERROR,
  POST_PROJECT_REQUEST,
  DEL_PROJECT_SUCCESS
} from '../constants';

const initialState = {
  data: [],
  errors: null,
  fetching: false
};

export default function PROJECT(state = initialState, action) {
  switch (action.type) {
    case POST_PROJECT_REQUEST:
    case GET_PROJECT_REQUEST:
      return { ...state, fetching: true };
    case GET_PROJECT_SUCCESS:
      return { ...state, data: action.PROJECT, fetching: false, errors: null };
    case POST_PROJECT_SUCCESS: {
      let PROJECT = [...state.data];
      PROJECT.push(action.doc);
      return { ...state, data: PROJECT, fetching: false, errors: null };
    }
    case PUT_PROJECT_SUCCESS: {
      let PROJECT = [...state.data];
      let doc = PROJECT.find(doc => doc._id === action.doc._id);
      for (let key in doc) {
        doc[key] = action.doc[key];
      }
      return { ...state, data: PROJECT, fetching: false, errors: null };
    }
    case DEL_PROJECT_SUCCESS: {
      let PROJECT = [...state.data].filter(doc => doc._id !== action.id);
      return { ...state, data: PROJECT, fetching: false, errors: null };
    }
    case GET_PROJECT_ERROR:
      return { ...state, errors: action.message, fetching: false };
    case DISMISS_PROJECT_ERROR:
      return { ...state, errors: null };
    default:
      return state;
  }
}
