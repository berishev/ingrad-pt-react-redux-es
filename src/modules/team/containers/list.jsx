import * as React from 'react';
import * as Components from '../../components/general';

export default class extends React.Component {
  renderColumns = () => {
    let columns = [
      {
        title: 'Название отдела',
        render: item => item.name
      },
      {
        title: 'Полное наименование отдела',
        render: item => item.fullname
      },
      {
        title: 'ИД',
        render: item => item.spOrg && item.spOrg.spID,
        width: 30
      }
    ];
    return columns;
  };

  renderHeader = () => {
    let { state, onAdd, onEdit, onRemove } = this.props.controller;
    let { selected } = state;
    let left = [
      <Components.Button key="1" buttonTitle="Добавить" onClick={onAdd} />,
      <Components.Button key="2" buttonTitle="Редактировать" disabled={selected.length != 1} onClick={onEdit} />,
      <Components.Button
        key="3"
        buttonTitle="Удалить"
        type="secondary"
        disabled={selected.length < 1}
        onClick={onRemove}
      />
    ];
    return <Components.ButtonBar left={left} />;
  };

  render() {
    const isAdmin = true;
    let { state, onSelect } = this.props.controller;
    let { query, selected } = state;
    return (
      <Components.ContentForm headerBar={isAdmin ? this.renderHeader() : null}>
        <Components.Table.QueryTable
          data={query}
          columns={this.renderColumns()}
          selection={{ items: selected, on: onSelect }}
          columnStyle={{ fontSize: 19 }}
          rowStyle={{ fontSize: 17 }}
        />
      </Components.ContentForm>
    );
  }
}
