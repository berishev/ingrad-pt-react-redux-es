import {
  GET_TEAM_REQUEST,
  POST_TEAM_REQUEST,
  DEL_TEAM_REQUEST,
  PUT_TEAM_REQUEST,
  DISMISS_TEAM_ERROR
} from '../constants';

export function get(TEAM_id, step_id) {
  return dispatch => {
    dispatch({
      type: GET_TEAM_REQUEST,
      TEAM_id,
      step_id
    });
  };
}

export function add(doc) {
  return dispatch => {
    dispatch({
      type: POST_TEAM_REQUEST,
      doc
    });
  };
}

export function save(doc) {
  return dispatch => {
    dispatch({
      type: PUT_TEAM_REQUEST,
      doc
    });
  };
}

export function del(id) {
  return dispatch => {
    dispatch({
      type: DEL_TEAM_REQUEST,
      id
    });
  };
}

export function dismissError() {
  return dispatch => {
    return dispatch({
      type: DISMISS_TEAM_ERROR
    });
  };
}
