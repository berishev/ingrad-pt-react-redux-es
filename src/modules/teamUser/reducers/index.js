import {
  GET_TEAMUSER_REQUEST,
  GET_TEAMUSER_SUCCESS,
  PUT_TEAMUSER_SUCCESS,
  POST_TEAMUSER_SUCCESS,
  GET_TEAMUSER_ERROR,
  DISMISS_TEAMUSER_ERROR,
  POST_TEAMUSER_REQUEST,
  DEL_TEAMUSER_SUCCESS
} from '../constants';

const initialState = {
  data: [],
  errors: null,
  fetching: false
};

export default function TEAMUSER(state = initialState, action) {
  switch (action.type) {
    case POST_TEAMUSER_REQUEST:
    case GET_TEAMUSER_REQUEST:
      return { ...state, fetching: true };
    case GET_TEAMUSER_SUCCESS:
      return { ...state, data: action.TEAMUSER, fetching: false, errors: null };
    case POST_TEAMUSER_SUCCESS: {
      let TEAMUSER = [...state.data];
      TEAMUSER.push(action.doc);
      return { ...state, data: TEAMUSER, fetching: false, errors: null };
    }
    case PUT_TEAMUSER_SUCCESS: {
      let TEAMUSER = [...state.data];
      let doc = TEAMUSER.find(doc => doc._id === action.doc._id);
      for (let key in doc) {
        doc[key] = action.doc[key];
      }
      return { ...state, data: TEAMUSER, fetching: false, errors: null };
    }
    case DEL_TEAMUSER_SUCCESS: {
      let TEAMUSER = [...state.data].filter(doc => doc._id !== action.id);
      return { ...state, data: TEAMUSER, fetching: false, errors: null };
    }
    case GET_TEAMUSER_ERROR:
      return { ...state, errors: action.message, fetching: false };
    case DISMISS_TEAMUSER_ERROR:
      return { ...state, errors: null };
    default:
      return state;
  }
}
