import {
  GET_TEAMUSER_REQUEST,
  POST_TEAMUSER_REQUEST,
  DEL_TEAMUSER_REQUEST,
  PUT_TEAMUSER_REQUEST,
  DISMISS_TEAMUSER_ERROR
} from '../constants';

export function get(TEAMUSER_id, step_id) {
  return dispatch => {
    dispatch({
      type: GET_TEAMUSER_REQUEST,
      TEAMUSER_id,
      step_id
    });
  };
}

export function add(doc) {
  return dispatch => {
    dispatch({
      type: POST_TEAMUSER_REQUEST,
      doc
    });
  };
}

export function save(doc) {
  return dispatch => {
    dispatch({
      type: PUT_TEAMUSER_REQUEST,
      doc
    });
  };
}

export function del(id) {
  return dispatch => {
    dispatch({
      type: DEL_TEAMUSER_REQUEST,
      id
    });
  };
}

export function dismissError() {
  return dispatch => {
    return dispatch({
      type: DISMISS_TEAMUSER_ERROR
    });
  };
}
