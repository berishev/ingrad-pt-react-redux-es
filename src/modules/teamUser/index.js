import form from './containers/form';
import list from './containers/list';

import actions from './actions';
import reducers from './reducers';

export default {
  form,
  list,
  redux: {
    actions,
    reducers
  }
};
