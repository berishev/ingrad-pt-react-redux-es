import * as Parse from 'parse';
import * as Model from '../../model';
import { Configs } from '../../components/filter';

export class FilterConfig extends Configs.ParseConfig {
  get projects() {
    if (!this.get('projects')) this.set('projects', []);
    return this.get('projects');
  }
  set projects(value) {
    this.set('projects', value);
  }
  get teams() {
    if (!this.get('teams')) this.set('teams', []);
    return this.get('teams');
  }
  set teams(value) {
    this.set('teams', value);
  }
  get relType() {
    if (!this.get('relType')) this.set('relType', []);
    return this.get('relType');
  }
  set relType(value) {
    this.set('relType', value);
  }
  get search() {
    return this.get('search');
  }
  set search(value) {
    this.set('search', value);
  }
  get filters() {
    let filters = super.filters;
    if (this.projects && this.projects.length) {
      filters.push(item => {
        let queriesProjects = this.projects.map(k => this.clone(item).equalTo('projects', k));
        return this.parseOr(...queriesProjects);
      });
    }
    if (this.teams && this.teams.length) {
      filters.push(item => {
        let queriesTeams = this.teams.map(k => this.clone(item).equalTo('team', k));
        return this.parseOr(...queriesTeams);
      });
    }
    if (this.relType && this.relType.length) {
      filters.push(item => {
        let quiriesRelType = this.relType.map(k => {
          let projectQuery = new Parse.Query(Model.Project).equalTo('relType', Model.ProjectRelTypeEnum[k]);
          return this.clone(item).matchesQuery('projects', projectQuery);
        });
        return this.parseOr(...quiriesRelType);
      });
    }
    if (this.search) {
      filters.push(item => {
        let queries = this.search
          .split(' ')
          .map(m =>
            this.parseOr(
              this.clone(item).matches('lastname', new RegExp(m, 'i'), null),
              this.clone(item).matches('name', new RegExp(m, 'i'), null),
              this.clone(item).matches('patronymic', new RegExp(m, 'i'), null)
            )
          );
        return this.parseAnd(...queries);
      });
    }
    return filters;
  }
}
