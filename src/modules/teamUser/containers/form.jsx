import * as React from 'react';
import * as Components from '../../components/general';
import * as Special from '../../components/special';

export default class extends React.Component {
  renderContent = () => {
    let { state, onChange, onSelect } = this.props.controller;
    let { item } = state;
    return (
      <>
        <Components.Divider>Личное</Components.Divider>
        <Special.Autocomplete.SPUserFIO
          fiTitle="ФИО"
          placeholder="Начните вводить ФИО"
          onSelect={onSelect}
          defaultValue={item && item.spUser && item.spUser.LinkTitle}
        />
        <Components.Row>
          <Components.Numberfield
            fiTitle="Добавочный"
            config={{
              object: item,
              path: 'phoneCode',
              afterSet: onChange
            }}
          />
          <Components.Phonefield
            fiTitle="Мобильный"
            config={{
              object: item,
              path: 'phoneMobile',
              afterSet: onChange
            }}
          />
        </Components.Row>
        <Components.Divider>Обязанности</Components.Divider>
        <Special.Selectfield.TeamType
          fiTitle="Отдел"
          config={{
            object: item,
            path: 'team',
            afterSet: onChange
          }}
        />
        <Special.Selectfield.TeamPositionType
          team={item.team}
          fiTitle="Проектная роль"
          config={{
            object: item,
            path: 'position',
            afterSet: onChange
          }}
        />
        <Components.Textfield
          fiTitle="Блок задач"
          config={{
            object: item,
            path: 'task',
            afterSet: onChange
          }}
        />

        <Components.Divider>Рабочее</Components.Divider>
        <Components.Textfield
          fiTitle="Офис"
          config={{
            object: item,
            path: 'officeNumber',
            afterSet: onChange
          }}
        />
        <Components.Textfield
          fiTitle="Этаж/Кабинет"
          config={{
            object: item,
            path: 'floorOrRoom',
            afterSet: onChange
          }}
        />
        <Special.Selectfield.ProjectTags
          fiTitle="Проекты"
          config={{
            object: item,
            path: 'projects',
            afterSet: onChange
          }}
        />
      </>
    );
  };
  renderHeader = () => {
    let { state, onCancel, onSave } = this.props.controller;
    let left = [
      <Components.Button
        key="1"
        buttonTitle="Сохранить"
        onClick={onSave}
        submit={{ validationObject: state.item, controller: this }}
      />,
      <Components.Button key="2" buttonTitle="Отменить" onClick={onCancel} type="secondary" />
    ];
    return <Components.ButtonBar left={left} />;
  };

  render() {
    return <Components.ContentForm headerBar={this.renderHeader()}>{this.renderContent()}</Components.ContentForm>;
  }
}
