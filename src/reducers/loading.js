import constants from '../constants';

export function requestFail(state = false, action) {
  switch (action.type) {
    case constants.REQUEST_FAIL:
      return action.hasError;
    default:
      return state;
  }
}

export function requestIsLoading(state = false, action) {
  switch (action.type) {
    case constants.REQUEST_IS_LOADING:
      console.log(action);
      return action.isLoading;
    default:
      return state;
  }
}

export function requestSuccess(state = {}, action) {
  switch (action.type) {
    case constants.REQUEST_SUCCESS:
      return action.data;
    default:
      return state;
  }
}
