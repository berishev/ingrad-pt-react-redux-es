import { combineReducers } from 'redux';
import { requestFail, requestIsLoading, requestSuccess } from './loading';

export default combineReducers({
  requestFail,
  requestIsLoading,
  requestSuccess
});
