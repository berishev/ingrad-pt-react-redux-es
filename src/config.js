export default {
  meta: {
    projectName: 'INGRAD',
    logoText: 'INGRAD'
  },
  color: {
    mainText: '#000',
    subText: '#d0031a',
    headerColor: '#fdfcff',
    headerTextColor: '#000',
    backgroundColor: '#fdfcff',
    backgroundColorGray: '#edeef0',
    divider: '#979797'
  },
  api: {
    appId: 'xhdbw4jfek4n2hh2dc87wdctnzugbdj8',
    jsKey: 'x8zqswgd3zv9k7q6ymuk5wn5h6pr76x5',
    // serverURL: 'http://localhost:9090/api',
    // serverURL: 'https://api.quadsales.ru/v1'
    serverURL: 'http://gis.ingrad.com:9090/v1'
  }
};
