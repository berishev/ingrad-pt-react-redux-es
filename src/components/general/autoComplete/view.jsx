import * as React from 'react';
import { withStyles, TextField, Paper, MenuItem } from '@material-ui/core';
import * as Autosuggest from 'react-autosuggest';
import styles from './styles';
import { FormItem, Tooltip } from '../abstract';
class AutoComplete extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = (value) => {
            this.setState({ value });
            if (this.props.onChange)
                this.props.onChange(value);
        };
        this.onSuggestionsFetchRequested = ({ value }) => {
            if (this.props.onFetch)
                this.props.onFetch(value);
        };
        this.renderSuggestion = (value, params) => {
            return (<MenuItem selected={params.isHighlighted} component="div">
        <div>
          <strong key={value} style={{ fontWeight: 300 }}>
            {value}
          </strong>
        </div>
      </MenuItem>);
        };
        this.renderInputComponent = (props) => {
            const { ref, inputRef = () => {
                /* */
            }, placeholder, ...other } = props;
            const input = (<TextField fullWidth placeholder={placeholder} InputProps={{
                inputRef: node => {
                    ref(node);
                    inputRef(node);
                }
            }} {...other}/>);
            return Tooltip(input, {
                placeholder,
                trigger: 'hover'
            });
        };
        this.state = {
            value: ''
        };
    }
    componentDidMount() {
        if (this.props.defaultValue != null &&
            this.props.defaultValue != this.state.value &&
            !this.state.value)
            this.onChange(this.props.defaultValue);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.defaultValue != null &&
            nextProps.defaultValue != this.state.value &&
            !this.state.value)
            this.onChange(nextProps.defaultValue);
    }
    render() {
        const { data, classes, placeholder } = this.props;
        const { value } = this.state;
        return (<Autosuggest suggestions={data} onSuggestionsFetchRequested={this.onSuggestionsFetchRequested} getSuggestionValue={(item) => item} renderSuggestion={this.renderSuggestion} renderInputComponent={this.renderInputComponent} theme={{
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion
        }} renderSuggestionsContainer={options => (<Paper {...options.containerProps} square>
            {options.children}
          </Paper>)} inputProps={{
            label: this.props.fiTitle,
            inputRef: node => {
                this['popperNode'] = node;
            },
            InputLabelProps: {
                shrink: true
            },
            placeholder: placeholder || 'Введите значение',
            value,
            onChange: (e, { newValue }) => this.onChange(newValue)
        }}/>);
    }
}
export default FormItem(withStyles(styles)(AutoComplete), { fiTitleType: 'None' });
