import * as React from 'react';
import { FormControl, FormLabel, InputLabel, FormHelperText, FormControlLabel } from '@material-ui/core';
import { AbstractComponent } from './abstractComponent';
import Decorators from '../../util/decorators';
import { LINQ } from 'berish-linq';
import Divider from '../divider';
class Item extends React.Component {
    render() {
        let formLabel = (<FormLabel error={this.props.fiStatus == 'error'} required={this.props.fiRequired}>
        {this.props.fiTitle}
      </FormLabel>);
        let inputLabel = (<InputLabel error={this.props.fiStatus == 'error'} required={this.props.fiRequired}>
        {this.props.fiTitle}
      </InputLabel>);
        let formControlLabel = (<FormControlLabel control={<>{this.props.children}</>} label={this.props.fiTitle}/>);
        let dividerLabel = <Divider>{this.props.fiTitle}</Divider>;
        return (<FormControl fullWidth={true} error={this.props.fiStatus == 'error'} required={this.props.fiRequired} style={{ margin: '8px 5px' }}>
        {this.props.fiTitleType == 'InputLabel' ? (<>
            {inputLabel}
            {this.props.children}
            {this.props.fiHelperText && (<FormHelperText error={this.props.fiStatus == 'error'} required={this.props.fiRequired}>
                {this.props.fiHelperText}
              </FormHelperText>)}
          </>) : this.props.fiTitleType == 'FormControlLabel' ? (formControlLabel) : this.props.fiTitleType == 'Divider' ? (<>
            {dividerLabel}
            {this.props.children}
            {this.props.fiHelperText && (<FormHelperText error={this.props.fiStatus == 'error'} required={this.props.fiRequired}>
                {this.props.fiHelperText}
              </FormHelperText>)}
            <Divider />
          </>) : this.props.fiTitleType == 'FormLabel' ? (<>
            {formLabel}
            {this.props.children}
            {this.props.fiHelperText && (<FormHelperText error={this.props.fiStatus == 'error'} required={this.props.fiRequired}>
                {this.props.fiHelperText}
              </FormHelperText>)}
          </>) : (<>
            {this.props.children}
            {this.props.fiHelperText && (<FormHelperText error={this.props.fiStatus == 'error'} required={this.props.fiRequired}>
                {this.props.fiHelperText}
              </FormHelperText>)}
          </>)}
      </FormControl>);
    }
}
export function FormItem(Component, formItemConfig) {
    formItemConfig = formItemConfig || {};
    let classItem = class FormItem extends AbstractComponent {
        render() {
            let { fiDisable, fiHelperText, fiRequired, fiStatus, fiTitle, fiTitleType, ...componentProps } = this.props;
            fiDisable = fiDisable != null ? fiDisable : formItemConfig.fiDisable;
            fiHelperText =
                fiHelperText != null ? fiHelperText : formItemConfig.fiHelperText;
            fiRequired = fiRequired != null ? fiRequired : formItemConfig.fiRequired;
            fiStatus = fiStatus != null ? fiStatus : formItemConfig.fiStatus;
            fiTitle = fiTitle != null ? fiTitle : formItemConfig.fiTitle;
            fiTitleType =
                fiTitleType != null ? fiTitleType : formItemConfig.fiTitleType;
            const component = (formItem) => (<Component {...formItem} {...componentProps}/>);
            if (!!fiDisable) {
                return component({
                    fiDisable,
                    fiHelperText,
                    fiRequired,
                    fiStatus,
                    fiTitle,
                    fiTitleType
                });
            }
            if (this.props.config)
                this.getValue();
            if (this.props.configs && this.props.configs.length > 0) {
                for (let config of this.props.configs) {
                    this.getValue(config.type);
                }
            }
            let model = this.props.config &&
                this.props.config.object &&
                Decorators.Filter.Validate(this.props.config.object, this.props.config.path);
            let filters = LINQ.fromArray((model && model.filters) || []);
            // model.filters.length == 0 - Убираем статус валидации на все элементы, у которых не установлены фильтры
            // filters.count(m => m.isGetExecute && !m.isSetExecute) > 0 - Убираем статус валидации для дефолтной формы,
            // у которых нет значений, но они на самом деле ошибочны
            // filters.all(m => m.isRequired == false) - Убираем статус валидации для полей, у которых установлены фильтры
            // (со статусом успешной валидации), но они могут быть пустыми(чтобы не отображать успешный статус валидации,
            // при необязательном статусе)
            if (filters.count() == 0 ||
                filters.count(m => m.isGetExecute && !m.isSetExecute && m.isError) >
                    0 ||
                (!model.isRequired && filters.count(m => m.isError) <= 0))
                filters = null;
            filters = filters && filters.where(m => m.isError && m.isSetExecute);
            let validateStatus = filters == null ? null : filters.count() > 0 ? 'error' : 'success';
            const formItemComponentConfig = {
                fiTitle,
                fiStatus: fiStatus || validateStatus,
                fiRequired: fiRequired != null ? fiRequired : model && model.isRequired,
                fiHelperText: fiHelperText ||
                    (filters && filters.select(m => m.errorText).lastOrNull()),
                fiDisable,
                fiTitleType
            };
            return (<Item {...formItemComponentConfig}>
          {component(formItemComponentConfig)}
        </Item>);
        }
    };
    return classItem;
}
