import * as React from 'react';
import { Search } from '@material-ui/icons';
import { AbstractComponent, FormItem } from './abstract';
import { Row, Button, Col, Textfield } from '..';
class SearchField extends AbstractComponent {
  constructor(props) {
    super(props);
    this.onSeacrh = () => {
      this.loadData(this.search);
    };
    this.search = async () => {
      let { onSearch } = this.props;
      let { searchText, text } = this.state;
      if (searchText == text) return;
      if (!onSearch) return;
      let result = await onSearch(text);
      searchText = text;
      this.setState({ searchText });
    };
    this.state = {
      text: '',
      searchText: '',
      loading: false
    };
  }
  async loadData(promise) {
    let loading = loading => new Promise(resolve => this.setState({ loading }, resolve));
    let result = null;
    await loading(true);
    result = await promise();
    await loading(false);
    return result;
  }
  render() {
    const input = (
      <Textfield
        fiTitle="Поиск"
        config={{
          object: this.state,
          path: 'text',
          afterSet: m => {
            if (!m.text) {
              m.searchText = m.text;
              if (this.props.onSearch) this.props.onSearch(null);
            }
            this.setState(m, this.props.searchEverySymbol ? () => this.onSeacrh() : null);
          }
        }}
        placeholder={this.props.placeholder || 'Введите значение для поиска'}
        onPressEnter={this.onSeacrh}
      />
    );
    if (this.props.searchEverySymbol) return input;
    return (
      <Row>
        <Col span={23}>
          <Row>{input}</Row>
        </Col>
        <Col span={1}>
          <Button
            loading={this.state.loading}
            onClick={this.onSeacrh}
            icon={<Search />}
            shape="circle"
            placeholder="Начать поиск"
          />
        </Col>
      </Row>
    );
  }
}
export default FormItem(SearchField, { fiTitleType: 'InputLabel' });
