import * as ringle from 'berish-ringle';
import guid from 'berish-guid';
class UploadMethodsImplementation {
    async parse(file, config) {
        const Parse = await import('parse');
        if (!Parse)
            throw 'Нет способа загрузки. Необходимо описать способ загрузки';
        let fileParse = new Parse.File(`${guid.generateId()}.${config.extension}`, file);
        fileParse = await fileParse.save();
        return fileParse.url();
    }
    async base64(file) {
        return new Promise((resolve, reject) => {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = err => reject(err);
        });
    }
    promtDownload(base64, filename, mimeType) {
        let element = document.createElement('a');
        element.setAttribute('href', `data:${mimeType};base64,${base64}`);
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
}
export default ringle.getSingleton(UploadMethodsImplementation);
