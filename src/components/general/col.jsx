import * as React from 'react';
export default class extends React.Component {
    render() {
        return <div style={{ display: 'flex', flex: this.props.span, flexDirection: 'column', ...this.props.style }}>{this.props.children}</div>;
    }
}
