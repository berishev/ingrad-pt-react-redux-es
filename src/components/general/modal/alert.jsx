import * as React from 'react';
import ModalForm from './form';
import { ContentForm, ButtonBar, Button, Row, Col } from '../';
export default class AlertModal extends React.Component {
    constructor() {
        super(...arguments);
        this.renderContent = () => {
            return <span>{this.props.text}</span>;
        };
        this.resolve = () => {
            this.props.resolve(true);
        };
        this.reject = () => {
            this.props.resolve(false);
        };
        this.renderFooter = () => {
            let left = [<Button key={0} fiTitle={this.props.okTitle || 'Да'} onClick={() => this.resolve()}/>, <Button key={1} fiTitle={this.props.cancelTitle || 'Нет'} onClick={() => this.reject()}/>];
            return <ButtonBar left={left}/>;
        };
    }
    render() {
        return (<ModalForm resolve={this.resolve} reject={this.reject} width="40vw">
        <ContentForm title={this.props.title} footerBar={this.renderFooter()}>
          <Row wrap="wrap" style={{ justifyContent: 'center', alignItems: 'center', textAlign: 'center', margin: 10 }}>
            <Col style={{ margin: 5 }}>
              <img src={`/images/${this.props.type}.png`}/>
            </Col>
            <Col style={{ margin: 5 }}>{this.props.text}</Col>
          </Row>
        </ContentForm>
      </ModalForm>);
    }
}
