import * as React from 'react';
import { LINQ } from 'berish-linq/dist';
import * as Model from '../../model';
import { Selectfield } from '../../general';
import { AbstractComponent } from '../../general/abstract';

export default class extends AbstractComponent {
  constructor(props) {
    super(props);
    this.onLoad = async nextProps => {
      let query = Model.SPOrgStructure.getQuery().limit(1000);
      let data = await query.find();
      this.setState({
        data
      });
    };
    this.renderData = () => {
      let { data } = this.state;
      return data
        .orderBy(m => m.spID)
        .select(m => {
          return {
            value: m,
            view: `${m.spID} - ${m.LinkTitle}`
          };
        });
    };
    this.state = {
      data: LINQ.fromArray([])
    };
  }
  //   componentDidMount() {
  //     executeController.tryLoad(this.onLoad);
  //   }
  render() {
    return <Selectfield placeholder="Выберите SP-Оргструтуру" data={this.renderData()} {...this.props} />;
  }
}
