import * as React from 'react';
import * as Model from '../../model';
import { AutoComplete } from '../../general';
import ParseAbstractConfig from '../../general/filter/configs/parse';
import AbstractConfig from '../../general/filter/config';

export default class extends React.Component {
  constructor() {
    super(...arguments);
    this.onFetch = async value => {
      let query = Model.SPUser.getQuery();
      let queries = value
        .split(' ')
        .map(m =>
          ParseAbstractConfig.parseOr(
            AbstractConfig.clone(query).matches('lastname', new RegExp(m, 'i'), null),
            AbstractConfig.clone(query).matches('firstname', new RegExp(m, 'i'), null),
            AbstractConfig.clone(query).matches('patronymic', new RegExp(m, 'i'), null)
          )
        );
      const endQuery = ParseAbstractConfig.parseAnd(...queries).limit(5);
      const items = await endQuery.find();
      return items.map(m => ({ value: m, text: m.LinkTitle }));
    };
    this.onSelect = async (text, value) => {
      if (this.props.onSelect) this.props.onSelect(value);
    };
  }
  render() {
    return <AutoComplete.Async {...this.props} onFetch={this.onFetch} onSelect={this.onSelect} />;
  }
}
