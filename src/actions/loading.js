import constants from '../constants';

export function requestFail(hasError) {
  return {
    type: constants.REQUEST_FAIL,
    hasError
  };
}

export function requestIsLoading(isLoading) {
  return {
    type: constants.REQUEST_IS_LOADING,
    isLoading
  };
}

export function requestSuccess(data) {
  return {
    type: constants.REQUEST_SUCCESS,
    data
  };
}

export function requestPromise(promise) {
  return async dispatch => {
    try {
      dispatch(requestIsLoading(true));
      // START LOAD
      const promiseData = promise instanceof Promise ? promise : promise();
      const data = await promiseData;
      // END LOAD
      dispatch(requestSuccess(data));
    } catch (err) {
      dispatch(requestFail(true));
    }
  };
}
