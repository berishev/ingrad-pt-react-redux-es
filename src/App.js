import * as React from 'react';
import { connect } from 'react-redux';
import { requestPromise, requestIsLoading } from './actions/loading';

class App extends React.Component {
  renderIsLoading = () => {
    return <h1>Loading {this.props.isLoading}</h1>;
  };

  renderIsError = () => {
    return <h1>Error {this.props.hasError}</h1>;
  };

  renderData = () => {
    return <h1>Data {this.props.data}</h1>;
  };

  onClick = () => {
    console.log('click');
    this.props.load('https://google.com/');
  };

  render() {
    console.log(this.props);
    if (this.props.hasError) return this.renderIsError();
    if (this.props.isLoading) return this.renderIsLoading();
    if (this.props.data) return this.renderData();
    return (
      <div className="App">
        <button onClick={this.onClick}>Click</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data,
    hasError: state.hasError,
    isLoading: state.isLoading
  };
};

async function test(data) {
  return new Promise(resolve => setTimeout(() => resolve(data), 1000));
}

const mapDispatchToProps = dispatch => {
  return {
    fetchData: url => dispatch(requestPromise(test(url))),
    load: () => dispatch(requestIsLoading(true))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
